﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtikalNasledjivanje
{
    class Torba : Artikal
    {
        public Artikal sadrzaj;

        public Torba(string naziv, double cena, Artikal sadrzaj):base(naziv, cena)
        {
            this.sadrzaj = sadrzaj;
        }
        public override void showDescription()
        {
            Console.WriteLine("Artikal smesten u torbi kod Alekse:");
            this.sadrzaj.showDescription();
        }
        public void put(Artikal a)
        {
            if (this.sadrzaj == null)
            {
                this.sadrzaj = a;
                Console.WriteLine("Artikal " + a.naziv + " je smesten u torbu!");
            }
            else
            {
                Console.WriteLine("Torba " + this.naziv + " je puna!");
            }
        }
        public void remove()
        {
            this.sadrzaj = null;
        }
    }
}
