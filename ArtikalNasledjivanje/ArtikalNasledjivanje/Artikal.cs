﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtikalNasledjivanje
{
    class Artikal
    {
        public string naziv;
        public double cena;

        public Artikal(string naziv, double cena)
        {
            this.naziv = naziv;
            this.cena = cena;
        }
        public virtual void showDescription()
        {
            Console.WriteLine("Naziv: " + this.naziv);
            Console.WriteLine("Cena: " + this.cena);
        }
        public double getPrice()
        {
            return this.cena;
        }
    }
}
