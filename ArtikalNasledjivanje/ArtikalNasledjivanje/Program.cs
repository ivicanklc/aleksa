﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtikalNasledjivanje
{
    class Program
    {
        static void Main(string[] args)
        {
            Laptop dell = new Laptop("dell", 145, "nije los", true);
            Torba torbice = new Torba("torbice", 20, dell);
            dell.showDescription();
            dell.turnOn();
            dell.turnOff();
            torbice.showDescription();
            torbice.put(dell);
            torbice.remove();
            torbice.put(dell);
            torbice.showDescription();
            Console.ReadLine();

        }
    }
}
