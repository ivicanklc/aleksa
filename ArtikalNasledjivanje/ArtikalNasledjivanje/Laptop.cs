﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArtikalNasledjivanje
{
    class Laptop : Artikal
    {
        public string opis;
        public bool stanje;

        public Laptop(string naziv, double cena, string opis, bool stanje) : base(naziv, cena)
        {
            this.opis = opis;
            this.stanje = stanje;
        }
        public override void showDescription()
        {
            base.showDescription();
            Console.WriteLine("Opis: " + this.opis);
        }
        public void turnOn()
        {
            this.stanje = true;
            Console.WriteLine("Laptop " + this.naziv + " je upaljen!");
        }
        public void turnOff()
        {
            this.stanje = false;
            Console.WriteLine("Laptop " + this.naziv + " je ugasen!");
        }
    }
}
